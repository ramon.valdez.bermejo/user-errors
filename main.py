# Scripts Final Project.

import sys
import re
import operator
import csv

def processLogs(fileName):
    searchPattern = r".* ticky: (ERROR|INFO) ([\w' ]+) (?:(\[#\d+\] ))?\((.*)\)$"
    per_error = {}
    per_user = {}

    print("== Parsing Logs file ==")
    lineNumber = 0
    #Iterate over logs
    with open(fileName, "r") as file:
        for line in file:
            matchingGroups = re.search(searchPattern, line)
            errorType = matchingGroups.group(1)
            error = matchingGroups.group(2)
            user = matchingGroups.group(4)
            lineNumber += 1

            #Errors
            if error in per_error.keys():
                per_error[error] += 1
            else:
                per_error[error] = 1
            #Users
            if user in per_user.keys():
                per_user[user][errorType] += 1
            else:
                per_user[user] = {'INFO': 0, 'ERROR': 0}
                per_user[user][errorType] = 1

    print("== Finished parsing Logs file ==")

    sorted_list_per_error = sorted(per_error.items(), key=operator.itemgetter(1), reverse=True)
    sorted_list_per_user = sorted(per_user.items(), key=operator.itemgetter(0))
    #Headers
    errors_header = ('ERROR', 'COUNT')
    #sorted_list_per_error.insert(0, errors_header)

    #Writing files
    print('Writing file')
    print(sorted_list_per_user)

    with open("C:/Users/ramon.valdez/PycharmProjects/ScriptsFinalProject/error_message.csv", "w", newline='') as file:
        csv_out = csv.writer(file)
        csv_out.writerow(['Error', 'Count'])
        csv_out.writerows(sorted_list_per_error)
    file.close()

    with open("C:/Users/ramon.valdez/PycharmProjects/ScriptsFinalProject/user_statistics.csv", "w") as file:
        file.write(f'Username, INFO, ERROR\n')
        for index, tuple in enumerate(sorted_list_per_user):
            file.write(f"{tuple[0]}, {tuple[1]['INFO']}, {tuple[1]['ERROR']}\n")
    file.close()

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    processLogs(sys.argv[1])

